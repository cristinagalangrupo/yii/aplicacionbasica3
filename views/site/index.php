<?php

use yii\helpers\Html;
use app\components\Foto;

/* @var $this yii\web\View */

$this->title = 'Página de inicio';
?>
<div class="site-index">

    <div class="jumbotron">
        <div class="body-content">
            <div class="row">
                <?php
                foreach ($fotosPortada as $dato) {
                    echo $this->render('unaPortada', [
                        'dato' => $dato,
                    ]);
                }
                ?>
            </div>

        </div>
    </div>

