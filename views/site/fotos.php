<?php
    use yii\grid\GridView;
    use yii\helpers\Html;
?>

<?= 

GridView::widget([
    'dataProvider'=>$dataProvider,
    'summary'=>"Mostrando {begin} - {end} de {totalCount} elementos",
     'tableOptions' => [
         'class' => 'table table-striped table-bordered fondoAzul',
         'style'=>['width:500px']
         ],
    'columns'=>[
        //['class' => 'yii\grid\SerialColumn'],
        [
                       'attribute'=>'nombre',
                       
                       'format'=>'raw',
                        'value' => function ($datos) {
                            $url = "@web/imgs/".$datos->nombre;
                            return Html::img($url, ['alt'=>'myImage','width'=>'500px']);
                        }
                        ],
    ]
]);
    ?>