<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Fotos;
use app\models\Paginas;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;

class SiteController extends Controller
{
    
    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $fotosportada = Fotos::find()
            ->where(['portada' => 1])
            ->all();
        
        return $this->render('index',[
            'fotosPortada'=>$fotosportada,
        ]);
    }
    
    public function actionFotos()
    {
        $fotosportada = Fotos::find()
            ->where(['portada' => 0]);
            
        $dataProvider = new ActiveDataProvider([
            'query' => $fotosportada,
            'pagination'=>[
                'pageSize'=>1,
            ]
        ]);

        return $this->render('fotos', [
            'dataProvider' => $dataProvider,
        ]);
        
        /*return $this->render('index',[
            'fotosPortada'=>$fotosportada,
        ]);*/
    }
    /*
    public function actionFotos()
    { 

         $count = Yii::$app->db->createCommand('
            SELECT COUNT(*) FROM fotos WHERE portada=:portada
        ', [':portada' => 0])->queryScalar();


        $dataProvider = new SqlDataProvider([
            'sql' => 'SELECT * FROM fotos WHERE portada=:portada',
            'params' => [':portada' => 0],
            'totalCount' => $count,
            'pagination' => [
                'pageSize' => 1, // es como el limit
            ],
        ]);
        var_dump($dataProvider);
        exit;

        // returns an array of data rows
        //$models = $dataProvider->getModels();
        //var_dump($models);
        
        //exit;
     
       
        return $this->render('fotos', [
            'dataProvider' => $dataProvider,
        ]);
        
      
       
       return $this->render('fotos',[
            'dataProvider'=>$models,
            
        ]);
    }
    */
    
    public function actionEstamos(){
        $pagina = Paginas::find()
            ->where(['nombre' => 'Dónde estamos'])
            ->one();
        return $this->render('estamos',[
            'pagina'=>$pagina
        ]);
    }
    public  function actionPagina($p){
        $pagina = Paginas::find()
            ->where(['nombre' => 'Página '.$p])
            ->one();
        return $this->render('pagina',[
            'pagina'=>$pagina,
        ]);
    }
    public  function actionPagina2(){
        $pagina = Paginas::find()
            ->where(['nombre' => 'Página 2'])
            ->one();
        return $this->render('pagina',[
            'pagina'=>$pagina,
        ]);
    }
    public  function actionPagina3(){
        $pagina = Paginas::find()
            ->where(['nombre' => 'Página 3'])
            ->one();
        return $this->render('pagina',[
            'pagina'=>$pagina,
        ]);
    }
        
}
