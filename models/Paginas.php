<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "paginas".
 *
 * @property int $id
 * @property string $nombre
 * @property string $texto
 */
class Paginas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'paginas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'texto'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'texto' => 'Texto',
        ];
    }
}
