DROP DATABASE IF EXISTS aplicacionbasica3;
CREATE DATABASE aplicacionbasica3;
USE aplicacionbasica3;

CREATE TABLE fotos(
  id int AUTO_INCREMENT,
  nombre varchar(255),
  portada bool, 
  PRIMARY KEY(id)
  );
  CREATE TABLE paginas(
    id int AUTO_INCREMENT,
    nombre varchar(255),
    texto text,
    PRIMARY KEY(id)
    );
INSERT INTO fotos (nombre,portada) VALUES
  ('foto1.jpg',1),
  ('foto2.jpg',0),
  ('foto3.jpg',1),
  ('foto4.jpg',0),
  ('foto5.jpg',1);
INSERT INTO paginas(nombre,texto) VALUES
  ('D�nde estamos','Estamos en Alpe Formaci�n'),
  ('P�gina 1', 'Es es el texto de la p�gina 1'),
  ('P�gina 2', 'Es es el texto de la p�gina 2'),
  ('P�gina 3', 'Es es el texto de la p�gina 3');
